<?php

namespace Drupal\webform_gbni\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElementBase;

/**
 * Provides a 'webform_british_national_insurance_number'.
 *
 * @WebformElement(
 *   id = "webform_british_national_insurance_number",
 *   label = @Translation("National Insurance Number (NINO)"),
 *   description = @Translation("Provides a field for a National Insurance Number (NINO)"),
 *   category = @Translation("Custom"),
 *   states_wrapper = TRUE,
 * )
 */
class WebformBritishNationalInsuranceNumber extends WebformElementBase {

  /**
   * {@inheritdoc}
   */
  public function getDefaultProperties() {
    return parent::getDefaultProperties() + [
      'multiple' => '',
      'size' => '',
      'minlength' => '',
      'maxlength' => '',
      'placeholder' => '',
      'allow_trn' => FALSE,
      'error_message' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['gbni'] = [
      '#type' => 'details',
      '#title' => $this->t('National Insurance Number (NINO)'),
      '#open' => TRUE,
    ];

    $form['gbni']['allow_trn'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow temporary reference number (TRN)'),
      '#description' => $this->t('The TRN is an HMRC reference number which allows the individual to pay tax/NICs it is not a NINO.
        <a href="https://www.gov.uk/hmrc-internal-manuals/national-insurance-manual/nim39110">Read more</a>'),
    ];

    $form['gbni']['error_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Error message'),
      '#description' => $this->t('This message will be shown when the user fills in an invalid NINO.'),
      '#required' => TRUE,
    ];
    return $form;
  }

}
