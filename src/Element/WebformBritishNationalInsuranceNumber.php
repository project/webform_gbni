<?php

namespace Drupal\webform_gbni\Element;

use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'webform_british_national_insurance_number'.
 *
 * @FormElement("webform_british_national_insurance_number")
 */
class WebformBritishNationalInsuranceNumber extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);

    $element = [
      '#input' => TRUE,
      '#type' => 'textfield',
      '#element_validate' => [
        [$class, 'validateNationalInsuranceNumber'],
      ],
      '#pre_render' => [
        [$class, 'preRenderNationalInsuranceNumber'],
      ],
      '#theme' => 'input__webform_example_element',
      '#theme_wrappers' => ['form_element'],
    ];
    return $element;
  }

  /**
   * Webform element validation handler.
   */
  public static function validateNationalInsuranceNumber(&$element, FormStateInterface $form_state, &$complete_form) {
    $nino_number = $form_state->getValue($element['#webform_key']);

    $nino_regex = '/(?![dfiquDFIQU])[a-zA-Z](?![dfiquoDFIQUO])[a-zA-Z](?<![bB][gG]|[gG][bB]|[kK][nN]|[nN][kK]|[nN][tT]|[tT][nN]|[zZ][zZ])[\d]{6}[abcdABCD]/m';
    preg_match($nino_regex, $nino_number, $matches);

    if (count($matches) == 0 && $element['#allow_trn']) {
      $trn_regex = '/[\d]{2}[a-zA-Z][\d]{6}/m';
      preg_match($trn_regex, $nino_number, $matches);
    }

    if (count($matches) == 0) {
      $form_state->setError($element, $element['#error_message']);
    }
  }

  /**
   * Prepares a render element for theme_element().
   */
  public static function preRenderNationalInsuranceNumber(array $element) {
    $element['#attributes']['type'] = 'text';
    $element['#attributes']['maxlength'] = 9;
    Element::setAttributes($element, [
      'id',
      'name',
      'value',
      'size',
      'maxlength',
      'placeholder',
    ]);
    static::setAttributes($element, [
      'form-text',
      'gbni',
      'js-webform-input-mask',
    ]);
    return $element;
  }

}
