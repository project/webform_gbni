# INTRODUCTION
This module adds a custom webform element for National Insurance number (NINO).

## REQUIREMENTS
-  `drupal/webform`

## INSTALLATION
`composer require drupal/webform_gbni`

## CONFIGURATION
No module wide configuration, field settings to be configured on individual
forms.

## DOCUMENTATION
Please refer to the official
[GOV.UK documentation](https://www.gov.uk/hmrc-internal-manuals/national-insurance-manual/nim39110)
for acceptable formats.


## CREDITS
Massive credits to FreMun as his project
https://www.drupal.org/project/webform_rrn_nrn provides the base template for
this module.
